defmodule Pantheon.Ruleset.V5Test do
  use ExUnit.Case
  doctest Pantheon.Ruleset.V5

  test "Will roll dice with pool, difficulty, hunger" do
    assert Pantheon.Ruleset.V5.handle_message("4d1h>=2") == {"Success", 0, [8, 5, 9, 2]}
  end

  test "Will roll dice without hunger or difficulty and defaults them to zero" do
    assert Pantheon.Ruleset.V5.handle_message("4d") == {"Success", 2, [8, 5, 9, 2]}
  end

  test "Will return an error on a malformed message" do
    assert Pantheon.Ruleset.V5.handle_message("4") == :error
  end
end
