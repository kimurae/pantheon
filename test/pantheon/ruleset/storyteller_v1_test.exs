defmodule Pantheon.Ruleset.StorytellerV1Test do
  use ExUnit.Case
  doctest Pantheon.Ruleset.V5

  alias Pantheon.Ruleset.StorytellerV1

  test "Will roll dice with pool, difficulty, and spec" do
    assert StorytellerV1.handle_message("4>=5spec") == {"Success", 3, [8, 5, 9, 2]}
  end

  test "Will roll dice with pool, and default to difficulty 6 and no spec" do
    assert StorytellerV1.handle_message("4") == {"Success", 2, [8, 5, 9, 2]}
  end
end
