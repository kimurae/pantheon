defmodule Pantheon.Ruleset.BasicTest do
  use ExUnit.Case
  doctest Pantheon.Ruleset.Basic

  alias Pantheon.Ruleset.Basic

  test "Will roll dice with pool, size, and difficulty" do
    assert Basic.handle_message("4d6>=2") == {"Success", 18, [4, 5, 5, 6]}
  end

  test "Will roll dice without difficulty and defaults them to zero" do
    assert Basic.handle_message("4d6") == {20, [4, 5, 5, 6]}
  end

  test "Will return an error on a malformed message" do
    assert Basic.handle_message("4") == :error
  end
end
