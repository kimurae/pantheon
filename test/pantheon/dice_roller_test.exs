defmodule Pantheon.DiceRollerTest do
  use ExUnit.Case
  doctest Pantheon.DiceRoller

  @rules [
    {:explode,:always, 8},
    {:reroll,:always, 5},
    {:reroll,:once, 9},
    {:explode,:once, 3}
  ]

  # The actual raw roll is: [2,9,5,9,2], due note right most is the first die rolled.
  # The 9 rerolls once into an 8 witch explodes into [8,3], as the three is explode once it remains.
  # The 5 rerolls into a 1.
  # The 9 rerolls into a 9, and thus once.
  test "it should roll dice" do
    assert Pantheon.DiceRoller.roll(5, 10, @rules) == [10, 2, 9, [8, 3, 1], 2] 
  end
end
