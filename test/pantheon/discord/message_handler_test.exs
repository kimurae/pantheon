defmodule Pantheon.Discord.MessageHandlerTest do
  use ExUnit.Case
  doctest Pantheon.Discord.MessageHandler

  alias Pantheon.Discord.MessageHandler

  test "handles messages sent to v5" do
    expected_result =
      %Nostrum.Struct.Embed{
        author: nil,
        color: nil,
        description: nil,
        fields: [
          %Nostrum.Struct.Embed.Field{inline: nil, name: "Result", value: "Success"},
          %Nostrum.Struct.Embed.Field{inline: nil, name: "Margin", value: "0"},
          %Nostrum.Struct.Embed.Field{inline: nil, name: "Rolled Dice", value: "`3,8,5,9,2`"}
        ],
        footer: nil,
        image: nil,
        provider: nil,
        thumbnail: nil,
        timestamp: nil,
        title: "Adam prays to the RNG gods",
        type: nil,
        url: nil,
        video: nil
      }

    assert MessageHandler.handle_message("!v5 5d1h>=2 Adam prays to the RNG gods") == {:ok, expected_result}
  end
end
