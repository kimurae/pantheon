FROM elixir:alpine AS compile-build

ENV MIX_ENV=prod

USER $USER

COPY . /app

WORKDIR /app

RUN mix "do" local.rebar --force, local.hex --force, deps.get, compile, release

FROM alpine:latest AS release-build

ENV MIX_ENV=prod

RUN apk add --no-cache --update \
 libstdc++ \
 ncurses-libs \
 && addgroup pantheon \
 && adduser pantheon -G pantheon -S

COPY --chown=pantheon:pantheon --from=compile-build /app/_build/prod /app

USER pantheon
WORKDIR /app

CMD ["/app/rel/pantheon/bin/pantheon", "start"]
