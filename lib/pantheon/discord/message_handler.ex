defmodule Pantheon.Discord.MessageHandler do
  import Nostrum.Struct.Embed

  require Logger

  @moduledoc """
  Handles incoming messages from discord, and returns embedded messages.
  """

  @command_text """
    * **Basic:** !r [pool]**d**[sides]>=[difficulty] [title] example: `!r 6d6>=10` Six six-sided Dice, difficulty 10
    * **Soryteller V1 (World of Darkness):** !st1 [pool]>=[difficulty]spec [title] example: `!st1 6>=8spec` Six Dice, difficulty 8, with specialization.
    * **Vampire 5th Edition:** !v5 [pool]**d**[hunger]**h**>=[difficulty] [title] example: `!v5 6d1h>=3` Six Dice, 1 hunger, 3 difficulty 
  """

  alias Pantheon.Ruleset.{Basic,StorytellerV1, V5}

  @spec handle_message(msg :: String.t()) :: {:ok, Nostrum.Struct.Embed.t()}
  def handle_message("!r " <> msg),   do: build_embed(Basic, msg)

  @spec handle_message(msg :: String.t()) :: {:ok, Nostrum.Struct.Embed.t()}
  def handle_message("!st1 " <> msg), do: build_embed(StorytellerV1, msg)

  @spec handle_message(msg :: String.t()) :: {:ok, Nostrum.Struct.Embed.t()}
  def handle_message("!v5 " <> msg),  do: build_embed(V5, msg)

  @spec handle_message(String.t()) :: {:ok, Nostrum.Struct.Embed.t()}
  def handle_message("!help") do
    embed =
      %Nostrum.Struct.Embed{}
      |> put_title("Help")
      |> put_field("Commands", @command_text)
      |> put_field("Pool", "The number of dice you will roll.")
      |> put_field("Sides", "The number of sides the dice will have, typically 6, 10, or 20 sided.")
      |> put_field("Difficulty", "The number of successes OR the target number you must meet or exceed for a success.")
    {:ok, embed}
  end

  def handle_message(_msg), do: :ignore

  defp build_embed(module, msg) do

    [command | description] = String.split(msg, " ")

    embed = 
      %Nostrum.Struct.Embed{}
      |> put_title(Enum.join(description, " "))
      |> inject_fields(module.handle_message(command))

    Logger.debug "#{inspect(embed)}"
    {:ok, embed}
  end

  defp inject_fields(embed, {status, margin, rolled_dice}) do
    embed
    |> put_field("Result", status)
    |> put_field("Margin", "#{margin}")
    |> put_field("Rolled Dice", "`#{Enum.join(rolled_dice, ",")}`")
  end

  defp inject_fields(embed, {result, rolled_dice})
  when is_number(result)
  when is_list(rolled_dice)
  do
    embed
    |> put_field("Result", "#{result}")
    |> put_field("Rolled Dice", "`#{Enum.join(rolled_dice, ",")}`")
  end

  defp inject_fields(embed, _) do
    put_field(embed, "Error", "Unable to parse command.")
  end
end
