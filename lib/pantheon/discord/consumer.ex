defmodule Pantheon.Discord.Consumer do
  use Nostrum.Consumer

  alias Nostrum.Api
  alias Pantheon.Rulesets

  import Nostrum.Struct.Embed

  def start_link() do
    Consumer.start_link(__MODULE__)
  end

  # Callbacks

  @impl true
  def handle_event({:MESSAGE_CREATE, msg, _ws_state}) do
    with {:ok, embed} <- Pantheon.Discord.MessageHandler.handle_message(msg.content) do
      Api.create_message(msg.channel_id, embed: embed)
    end
  end

  @impl true
  def handle_event(_event) do
    :noop
  end
end
