defmodule Pantheon.Application do
  @moduledoc """
  Entrypoint for the Pantheon Application
  """

  use Application

  @impl true
  @spec start(
    Application.start_type(),
    term()
  ) :: {:ok, pid()} | {:ok, pid(), Application.state()} | {:error, term()}
  def start(_start_type, _args) do
    children = [
      Pantheon.Discord.Supervisor,
      {Plug.Cowboy, scheme: :http, plug: Pantheon.HealthPlug, options: Application.get_env(:pantheon, Pantheon.HealthPlug, [port: 4001])}
    ]

    Supervisor.start_link(children, [strategy: :rest_for_one, name: Pantheon.Supervisor])
  end
end
