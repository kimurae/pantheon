defmodule Pantheon.DiceRoller do
  require Logger

  @moduledoc """
  Generates randomized integers to simulate Dice Rolling.
  """

  @typedoc """
  A rule that define the rolling behavior for the dice.

  The rule consists of three parts:
  * An action
  * The frequency of the action.
  * The target number that triggers the action.

  ## Actions
  * `:explode` Another dice will be added to the pool, rolled and grouped with the exploding die.
  * `:reroll` The die will be replaced with a new dice that is then rolled.

  ## Frequency
  * `:once` The action is done once for this die.
  * `:always` The action can repeat itself until it no longer can perform the action.

  """
  @type rule :: {action :: (:explode | :reroll), frequency :: (:always | :once), target :: pos_integer()}

  @doc """
  Rolls dice.

  Returns `rolled_dice()`

  ## Example:
      iex> Pantheon.DiceRoller.roll(2, 10, [])
      [9,2]
  """  
  @spec roll(pool :: pos_integer(), sides :: pos_integer(), rules :: list(rule())) :: list(pos_integer() | list(pos_integer()))
  def roll(pool, sides, rules \\ [])
  when pool >= 0
  and pool <= 100
  and sides > 0
  and sides <= 100
  and is_list(rules)
  do
    :ok = generate_seed!()

    roll_dice_pool([], pool, sides, rules)
  end

  defp config() do
    Application.get_env(:pantheon, __MODULE__, [seed: nil])
  end

  defp generate_seed!() do
    if config()[:seed] do
      :rand.seed(:exro928ss, config()[:seed]) # Mostly used for unit tests to override seed generation.
    else
      << i1 :: unsigned-integer-32, i2 :: unsigned-integer-32, i3 :: unsigned-integer-32 >> = :crypto.strong_rand_bytes(12)

      :rand.seed(:exro928ss, {i1, i2, i3})
    end

    :ok
  end

  defp roll_dice_pool(rolled_dice, 0, _sides, _rules), do: rolled_dice

  defp roll_dice_pool(rolled_dice, pool, sides, rules) do
    [ roll_single_die(sides, rules) | rolled_dice ]
    |> roll_dice_pool(pool - 1, sides, rules)
  end

  defp roll_single_die(sides, rules, roll_count \\ 0) do

    result  = :rand.uniform(sides)

    {rule, frequency, _} = Enum.find(rules, {:ok, :ok, :ok}, fn {_, _, target} -> result == target end)

    case {rule, frequency, roll_count} do
      {:explode,:always, _} ->
        Logger.debug("#{result} explodes!")
        List.flatten([result, roll_single_die(sides, rules)])
      {:explode,:once, 0} ->
        Logger.debug("#{result} explodes!")
        List.flatten([result, roll_single_die(sides, rules, roll_count + 1)])
      {:reroll,:always, _} ->
        Logger.debug("#{result} rerolls")
        roll_single_die(sides, rules)
      {:reroll,:once, 0} ->
        Logger.debug("#{result} rerolls")
        roll_single_die(sides, rules, roll_count + 1)
      _ ->
        Logger.debug("#{result} received!")
        result
    end
  end
end
