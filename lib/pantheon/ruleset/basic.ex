defmodule Pantheon.Ruleset.Basic do
  @moduledoc """
  Ruleset for basic rolls.
  """

  use Pantheon.Ruleset, message_mask: ~r/^(?<pool>\d{1,2})d(?<sides>(\d{1,2}|100))(>=(?<difficulty>\d+)|)$/
end
