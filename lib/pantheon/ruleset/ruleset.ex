defmodule Pantheon.Ruleset do

  @moduledoc """
  Ruleset behavior that determines how the dice are treated.
  """

  @doc """
  Tallies the die and determines the results.
  """
  @callback calculate_results(rolled_dice :: list(pos_integer() | list(pos_integer())), roll_opts :: map()) :: result()

  @doc """
  Handles the message passed in from the discord consumer.
  """
  @callback handle_message(msg :: String.t()) :: result()

  @type result :: {status :: String.t(), margin :: number(), rolled_dice :: list(pos_integer())}

  defmacro __using__(opts) do

    message_mask = Keyword.get(opts, :message_mask)
    sides        = Keyword.get(opts, :sides)
    rules        = Keyword.get(opts, :rules, [])

    quote do
      @behaviour Pantheon.Ruleset

      @doc false
      def calculate_results(rolled_dice, %{"difficulty" => difficulty} = roll_opts) do

        flattened_dice = Enum.map(rolled_dice, fn x ->
          if is_list(x) do
            Enum.sum(x)
          else
            x
          end
        end)

        total   = Enum.sum(flattened_dice)
        margin  = total - difficulty

        cond do
          difficulty == 0 ->
            {margin, flattened_dice}
          total >= difficulty ->
            {"Success", margin, flattened_dice}
          true ->
            {"Failure", margin, flattened_dice}
        end
      end

      @doc false
      def handle_message(msg)
      when is_binary(msg)
      do
        if Regex.match?(unquote(message_mask), msg) do
          roll_opts = Pantheon.Ruleset.parse_message(msg, unquote(message_mask))

          Pantheon.DiceRoller.roll(roll_opts["pool"] || 0, roll_opts["sides"] || unquote(sides), unquote(rules))
          |> calculate_results(roll_opts)

        else
          :error
        end
      end

      defoverridable calculate_results: 2, handle_message: 1
    end
  end

  @doc false
  def parse_message(msg, message_mask) do
    Regex.named_captures(message_mask, msg)
    |> Map.new(fn {k,v} -> {k, parse_value(v)} end)
  end

  defp parse_value(""), do: 0

  defp parse_value(value)
  when is_binary(value)
  do
    if Regex.match?(~r/\d+/, value) do
      String.to_integer(value)
    else
      value
    end
  end
end
