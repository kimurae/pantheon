defmodule Pantheon.Ruleset.StorytellerV1 do
  @moduledoc """
  Ruleset for white wolf's Storyteller system used in the first 4 versions of world of darkness.
  """

  use Pantheon.Ruleset,
    message_mask: ~r/^(?<pool>\d+)(>=(?<difficulty>\d+)|)((?<spec>spec)|)$/,
    sides: 10

  @impl true
  def calculate_results(rolled_dice, %{"difficulty" => difficulty, "spec" => spec} = _roll_opts) do
    success_count = if difficulty == 0 do
      Enum.count(rolled_dice, fn x -> x >= 6 end)
    else
      Enum.count(rolled_dice, fn x -> x >= difficulty end)
    end

    margin = Enum.sum([
      success_count,
      if spec == "spec" do
        Enum.count(rolled_dice, fn x -> x == 10 end)
      else
        0
      end,
      Enum.count(rolled_dice, fn x -> x == 1 end) * -1
    ])

    status =
      cond do
        margin > 0 ->
          "Success"
        margin < 0 && success_count == 0 ->
          "Botch"
        true ->
          "Failure"
      end

    {status, margin, rolled_dice}
  end
end
