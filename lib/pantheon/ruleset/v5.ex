defmodule Pantheon.Ruleset.V5 do

  use Pantheon.Ruleset,
    message_mask: ~r/^(?<pool>\d+)d((?<hunger>\d)h|)(>=(?<difficulty>\d+)|)\w*$/,
    sides: 10

  # need to format the rolled dice in some fashion
  @impl true
  def calculate_results(rolled_dice, %{"difficulty" => difficulty, "hunger" => hunger} = _roll_opts)
  when is_list(rolled_dice)
  and is_number(difficulty)
  and is_number(hunger)
  do
    hunger_dice = Enum.take(rolled_dice, hunger)

    successes = Enum.count(rolled_dice, &(&1 > 5))

    margin = successes - difficulty

    number_of_tens = Enum.count(rolled_dice, &(&1 == 10))

    status = 
      cond do
        margin >= 0 && number_of_tens > 1 && Enum.member?(hunger_dice, 10) ->
          "Messy Critical"
        margin >= 0 && number_of_tens > 1 ->
          "Critical"
        margin >= 0 ->
          "Success"
        Enum.member?(hunger_dice, 1) ->
          "Bestial Failure"
        true ->
          "Failure"
      end

    {status, margin, rolled_dice}
  end
end
