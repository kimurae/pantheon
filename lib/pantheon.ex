defmodule Pantheon do
  @moduledoc """
  Documentation for `Pantheon`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Pantheon.hello()
      :world

  """
  def hello do
    :world
  end
end
