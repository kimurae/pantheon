# Pantheon

A dice bot for discord that supports multiple systems.

## Installation
Clone the repo, install the dependencies.

### Dependencies
> * [Discord Developer Account](https://discord.com/developers)
> * [Docker](https://www.docker.com)
> * [Go-Task](https://taskfile.dev/#/installation)
> * `git`
> * Some form of docker registry manager like Digital Ocean, DockerHub, or AWS ECR

## Usage

### Environment Variables
> The runtime of the bot expects these environment variables to be provided for container when executed.
>
> |                |                                                                                 |
> |----------------|---------------------------------------------------------------------------------|
> | DISCORD\_TOKEN | The token string you get from your Discord Bot (see Discord's Developer Portal) |
> | PORT           | Defaults to 4001, mostly used for the health checker.                           |

### Health Checks
> There is an endpoint at `/health` that returns a `200`.

### Tests
> Run the ExUnit test-suite.
>
> `task test`

### Deploying
> Deploy to a docker image registry.
>
> `task REGISTRY=<your registry> TAG=<image tag> deploy`

### Run locally
> Run the bot locally on your machine.
>
> `task run`

## Documentation
> Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
>
> Run `task doc`. This will generate both an epub and html site in the `./doc` directory.

