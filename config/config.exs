import Config

config :logger,
  backends: [:console]

config :logger, :console,
  format: "\n##### $time $metadata[$level] $levelpad$message\n",
  metadata: :all

config :porcelain, driver: Porcelain.Driver.Basic

import_config "#{config_env()}.exs"
